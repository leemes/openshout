#ifndef PLAYBACK_H
#define PLAYBACK_H

#include <QObject>
#include <QUrl>

class AbstractPlayback : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUrl url WRITE setUrl READ url NOTIFY urlChanged)
    Q_PROPERTY(qreal volume WRITE setVolume READ volume NOTIFY volumeChanged)
    Q_PROPERTY(Status status READ status NOTIFY statusChanged)
    Q_PROPERTY(QString streamTitle READ streamTitle NOTIFY streamTitleChanged)
    Q_PROPERTY(bool paused READ paused WRITE setPaused NOTIFY pausedChanged)
    Q_ENUMS(Status)

public:
    explicit AbstractPlayback(QObject *parent = 0);
    ~AbstractPlayback();

    enum Status {
        StatusIdle,
        StatusRequesting,
        StatusBuffering,
        StatusReady
    };

signals:
    void urlChanged(QUrl url);
    void volumeChanged(qreal volume);
    void statusChanged(Status status);
    void pausedChanged(bool paused);
    void streamTitleChanged(QString streamTitle);

public slots:
    QUrl url() const;
    qreal volume() const;
    Status status() const;
    bool paused() const;
    QString streamTitle() const;

    void setUrl(QUrl url);
    void setVolume(qreal volume);
    void setPaused(bool paused);

    void play();  // = setPause(false)
    void pause(); // = setPause(true)
    void stop();  // = setUrl(QUrl())

protected:
    void setStatus(Status status);
    void setStreamTitle(QString streamTitle);

    virtual void _load() = 0;
    virtual void _setVolume() = 0;
    virtual void _togglePause() = 0;

protected:
    QUrl m_url;
    qreal m_volume;
    Status m_status;
    bool m_paused;
    QString m_streamTitle;
};

#endif // PLAYBACK_H

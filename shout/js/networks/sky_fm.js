Qt.include('_common.js')

var name = 'SKY.fm Radio'
var homepage = 'http://www.sky.fm/'

function getChannels(htmlCode) {
    var result = []
    // every channel item starts with a "<li data-key="
    var blocks = htmlCode.split(/<li data-key=/)
    // don't look at first part (which is the stuff before the first "<li data-key=")
    for (var i = 1; i < blocks.length; ++i) {
        var htmlBlock = blocks[i]
        var channelNameMatch = /<p class="channel"><a [^>]*>([^<]*)/.exec(htmlBlock)
        if (channelNameMatch === null) return null;
        var channelName = decodeXMLText(channelNameMatch[1])

        var channelUrlMatch
        var channelUrl = ''
        channelUrlMatch = /<a href="\/([^"]*)"/.exec(htmlBlock)
        if (channelUrlMatch === null) return null;
        channelUrl = 'http://pub2.sky.fm/sky_' + channelUrlMatch[1] + '_aacplus?type=.flv'
        result.push({name: channelName, url: channelUrl})
    }
    return result
}

var init = function(add) {
    httpGet(homepage, function(http) {
        if (http.status !== 200)
            console.log("Failed to fetch webpage for " + name)
        else {
            var channelItems = getChannels(http.responseText)
            if (channelItems === null)
                console.log("Failed to parse webpage for " + name)
            else {
                for (var i = 0; i < channelItems.length; ++i)
                    add(channelItems[i])
            }
        }
    })
}

.pragma library

// Easy wrapper around XMLHttpRequest
function httpGet(url, callback) {
    var http = new XMLHttpRequest();
    http.open("GET", url);
    http.onreadystatechange = function() {
        if (http.readyState === XMLHttpRequest.DONE) {
            callback(http);
        }
    }
    http.send();
}

// Removes XML entities like '&#x32;' (currently only hex notation)
function decodeXMLText(xmlText) {
    return xmlText.replace(/&#x([0-9a-f]+);/, function(entity, hex) { return String.fromCharCode(parseInt(hex, 16)) })
}

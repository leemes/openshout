Qt.include('_common.js')

var name = 'Digitally Imported'
var homepage = 'http://www.di.fm/'

function getChannels(htmlCode) {
    var result = []
    // every channel item starts with a "<li data-channel-id=", indented with 16 spaces
    var blocks = htmlCode.split(/ {16}<li data-channel-id=/)
    // don't look at first part (which is the stuff before the first "<li data-channel-id=")
    for (var i = 1; i < blocks.length; ++i) {
        var htmlBlock = blocks[i]
        var channelNameMatch = /<span>([^<]*)<\/span>/.exec(htmlBlock)
        if (channelNameMatch === null) return null;
        var channelName = decodeXMLText(channelNameMatch[1])

        var channelUrlMatch
        var channelUrl = ''
        channelUrlMatch = /<a href="\/([^"]*)"/.exec(htmlBlock)
        if (channelUrlMatch === null) return null;
        channelUrl = 'http://listen.di.fm/public3/' + channelUrlMatch[1] + '.pls'
        result.push({name: channelName, url: channelUrl})
    }
    return result
}

var init = function(add) {
    httpGet(homepage, function(http) {
        if (http.status !== 200)
            console.log("Failed to fetch webpage for " + name)
        else {
            var channelItems = getChannels(http.responseText)
            if (channelItems === null)
                console.log("Failed to parse webpage for " + name)
            else {
                for (var i = 0; i < channelItems.length; ++i)
                    add(channelItems[i])
            }
        }
    })
}

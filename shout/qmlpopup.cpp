#include "qmlpopup.h"
#include <QMouseEvent>
#include <QDebug>
#include <QDeclarativeContext>
#include <QDeclarativeEngine>
#include <QGraphicsObject>
#include "qmlcontroller.h"


QmlPopup::QmlPopup(QDeclarativeView *client, const QScriptValue &callback) :
    QDeclarativeView(client),
    client(client),
    closeCallback(callback)
{
    connect(engine(), SIGNAL(quit()), SLOT(close()));
    setResizeMode(QDeclarativeView::SizeRootObjectToView);

    setWindowFlags(Qt::Popup | Qt::FramelessWindowHint);

    rootContext()->setContextProperty("client", client->rootObject());
    rootContext()->setContextProperty("controller", new QmlController(this));
}

bool QmlPopup::event(QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress) {
        QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
        if (!QRect(QPoint(0, 0), size()).contains(mouseEvent->pos()))
            close();
    }
    else if (event->type() == QEvent::Close) {
        closeCallback.call();
        deleteLater();
    }
    return QDeclarativeView::event(event);
}

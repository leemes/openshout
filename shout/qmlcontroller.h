#ifndef QMLCONTROLLER_H
#define QMLCONTROLLER_H

#include <QObject>
#include <QSharedPointer>
#include <QDeclarativeView>
#include <QScriptValue>
#include <QList>
#include <QUrl>

class QmlPopup;


class QmlController : public QObject
{
    Q_OBJECT

public:
    explicit QmlController(QDeclarativeView *view);

public slots:
    void close();
    void moveBy(int dx, int dy);
    QObject *showPopup(QScriptValue arguments);

private:
    QUrl urlFromRelativePath(const QString &fileName);
    QmlPopup* makePopup(const QString &fileName, const QScriptValue &onClose);

private slots:
    void viewStatusChanged(QDeclarativeView::Status);

private:
    QDeclarativeView *view;
};

#endif // QMLCONTROLLER_H

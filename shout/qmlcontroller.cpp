#include "qmlcontroller.h"
#include "qmlpopup.h"
#include <QDir>
#include <QDebug>
#include <QScriptValueIterator>
#include <QGraphicsObject>
#include <QApplication>
#include <QDesktopWidget>


QmlController::QmlController(QDeclarativeView *view) :
    QObject(view),
    view(view)
{
    Q_ASSERT(view);
    connect(view, SIGNAL(statusChanged(QDeclarativeView::Status)), SLOT(viewStatusChanged(QDeclarativeView::Status)));
}

void QmlController::close()
{
    view->close();
}

void QmlController::moveBy(int dx, int dy)
{
    view->move(view->x() + dx, view->y() + dy);
}

QObject *QmlController::showPopup(QScriptValue arguments)
{
    QList<QString> errors;
    QString fileName;
    QScriptValue onClose;
    QPoint position;
    QRect around;
    QSize size;

    // verify and parse variant arguments:
    if (!arguments.property("source").isValid())
        errors << "'source' argument is mandatory";
    if (!arguments.property("position").isValid()
            && !arguments.property("around").isValid())
        errors << "either 'position' or 'around' argument is mandatory";
    if (arguments.property("position").isValid()
            && arguments.property("around").isValid())
        errors << "not both 'position' and 'around' arguments can be given";
    QScriptValueIterator it(arguments);
    while (it.hasNext()) {
        it.next();
        QString n = it.name().toLower();
        const QScriptValue &v = it.value();
        if (n == "source")
        {
            if (!v.isString())
                errors << "'source' has to be a string";
            else
                fileName = v.toString();
        }
        else if (n == "onclose")
        {
            if (!v.isFunction())
                errors << "'onClose' has to be a function";
            else
                onClose = v;
        }
        else if (n == "position")
        {
            if (!v.isObject()
                    || !v.property("x").isValid()
                    || !v.property("y").isValid())
                errors << "'position' has to be a point";
            else
                position = QPoint(v.property("x").toInt32(),
                                  v.property("y").toInt32());
        }
        else if (n == "around")
        {
            if (!v.isObject()
                    || !v.property("x").isValid()
                    || !v.property("y").isValid()
                    || !v.property("width").isValid()
                    || !v.property("height").isValid())
                errors << "'around' has to be a rect";
            else
                around = QRect(v.property("x").toInt32(),
                               v.property("y").toInt32(),
                               v.property("width").toInt32(),
                               v.property("height").toInt32());
        }
        else if (n == "size")
        {
            if (!v.isObject()
                    || !v.property("width").isValid()
                    || !v.property("height").isValid())
                errors << "'size' has to be a size";
            else
                size = QSize(v.property("width").toInt32(),
                             v.property("height").toInt32());
        }
        else
        {
            errors << "No such argument: " + it.name();
        }
    }

    if (!errors.isEmpty()) {
        qDebug("%s: Illegal call to showPopup():", metaObject()->className());
        foreach (QString error, errors)
            qDebug("    %s", qPrintable(error));
        return 0;
    }

    QmlPopup* popup = makePopup(fileName, onClose);
    if (size.isValid())
        popup->resize(size);
    else
        size = popup->size();
    if (around.isNull()) {
        popup->move(view->mapToGlobal(position));
    } else {
        QRect screenRect = QApplication::desktop()->screenGeometry(view);
        qDebug() << screenRect;

        QPoint below = view->mapToGlobal(around.bottomLeft());
        QRect belowRect(below, size);
        if (screenRect.contains(belowRect))
            popup->move(below);
        else {
            QPoint above = view->mapToGlobal(around.topLeft() - QPoint(0, size.height()));
            QRect aboveRect(above, size);
            if (screenRect.contains(aboveRect))
                popup->move(above);
        }
    }
    popup->show();
    return popup->rootObject();
}

QUrl QmlController::urlFromRelativePath(const QString &fileName)
{
    QDir currentDir(view->source().toLocalFile());
    currentDir.cdUp();
    return QUrl::fromLocalFile(currentDir.filePath(fileName));
}

QmlPopup* QmlController::makePopup(const QString &fileName, const QScriptValue &onClose)
{
    QmlPopup* popup = new QmlPopup(view, onClose);
    popup->setSource(urlFromRelativePath(fileName));
    return popup;
}

void QmlController::viewStatusChanged(QDeclarativeView::Status status)
{
    if (status == QDeclarativeView::Ready) {
        if (view->windowFlags() & Qt::FramelessWindowHint) {
            QVariant radiusVariant = view->rootObject()->property("windowRadius");
            if ((QMetaType::Type)radiusVariant.type() == QMetaType::Int) {
                int r = radiusVariant.toInt();
                if (r) {
                    int w = view->width(), h = view->height();
                    r = qMin(r, qMin(w, h) / 2);
                    int d = 2 * r;

                    QRegion maskRegion(view->rect());
                    maskRegion -= QRegion(0, 0, r, r);
                    maskRegion += QRegion(0, 0, d, d, QRegion::Ellipse);
                    maskRegion -= QRegion(w-r, 0, r, r);
                    maskRegion += QRegion(w-d, 0, d, d, QRegion::Ellipse);
                    maskRegion -= QRegion(0, h-r, r, r);
                    maskRegion += QRegion(0, h-d, d, d, QRegion::Ellipse);
                    maskRegion -= QRegion(w-r, h-r, r, r);
                    maskRegion += QRegion(w-d, h-d, d, d, QRegion::Ellipse);

                    view->setMask(maskRegion);
                }
            }
        }
    }
}

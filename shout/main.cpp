#include <QApplication>
#include <QtDeclarative>
#include "qmlapplicationviewer.h"
#include "playbackmplayer.h"
#include "qmlcontroller.h"

#define BACKEND Mplayer

#define BACKEND_TYPE_(backend) Playback##backend
#define BACKEND_TYPE(backend) BACKEND_TYPE_(backend)
typedef BACKEND_TYPE(BACKEND) Playback;

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    QIcon appIcon("img/appicon.ico");

    qmlRegisterType<Playback>("shout", 1, 0, "Playback");

    QmlApplicationViewer viewer;
    QDeclarativeContext *qml = viewer.rootContext();
    QmlController *controller = new QmlController(&viewer);
    qml->setContextProperty("window", &viewer);
    qml->setContextProperty("controller", controller);

    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    //viewer.setWindowFlags(viewer.windowFlags() | Qt::FramelessWindowHint);
    viewer.setMainQmlFile(QLatin1String("qml/main.qml"));
    viewer.show();

    return app->exec();
}

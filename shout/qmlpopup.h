#ifndef QMLPOPUP_H
#define QMLPOPUP_H

#include <QDeclarativeView>
#include <QScriptValue>

class QmlPopup : public QDeclarativeView
{
    Q_OBJECT
public:
    /** \a client will become the parent */
    explicit QmlPopup(QDeclarativeView *client, const QScriptValue &closeCallback);
    
protected:
    bool event(QEvent *event);

private:
    QDeclarativeView *client;
    QScriptValue closeCallback;
};

#endif // QMLPOPUP_H

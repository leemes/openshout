#include "playback.h"

AbstractPlayback::AbstractPlayback(QObject *parent) :
    QObject(parent),
    m_volume(0.0),
    m_status(StatusIdle),
    m_paused(false)
{
}

AbstractPlayback::~AbstractPlayback()
{
}

QUrl AbstractPlayback::url() const
{
    return m_url;
}

qreal AbstractPlayback::volume() const
{
    return m_volume;
}

AbstractPlayback::Status AbstractPlayback::status() const
{
    return m_status;
}

bool AbstractPlayback::paused() const
{
    return m_paused;
}

QString AbstractPlayback::streamTitle() const
{
    return m_streamTitle;
}

void AbstractPlayback::setUrl(QUrl url)
{
    if (m_url != url) {
        m_url = url;
        _load();
        emit urlChanged(url);
    }
}

void AbstractPlayback::setVolume(qreal volume)
{
    if (m_volume != volume) {
        m_volume = volume;
        _setVolume();
        emit volumeChanged(volume);
    }
}

void AbstractPlayback::setPaused(bool paused)
{
    if (m_paused != paused) {
        m_paused = paused;
        _togglePause();
        emit pausedChanged(paused);
    }
}

void AbstractPlayback::play()
{
    setPaused(false);
}

void AbstractPlayback::pause()
{
    setPaused(true);
}

void AbstractPlayback::stop()
{
    setUrl(QUrl());
}

void AbstractPlayback::setStatus(AbstractPlayback::Status status)
{
    if (m_status != status) {
        m_status = status;
        emit statusChanged(status);
    }
}

void AbstractPlayback::setStreamTitle(QString streamTitle)
{
    if (m_streamTitle != streamTitle) {
        m_streamTitle = streamTitle;
        emit streamTitleChanged(streamTitle);
    }
}



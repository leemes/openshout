import QtQuick 1.1
import shout 1.0

Item {
    id: fadedPlayback

    property real fadeDuration: 500
    property real volume: 1.0

    property string streamTitle: backend.streamTitle

    property bool playing: backend.status === Playback.StatusReady && backend.paused === false
    property bool buffering: backend.status === Playback.StatusRequesting || backend.status === Playback.StatusBuffering

    function play(url) {
        backend.url = url
    }
    function stop() {
        backend.fadeOut()
    }
    signal starting(); // emitted when playback starts



    Playback {
        id: backend

        property real volumeFactor: 0.0
        onUrlChanged: volumeFactor = 0.0

        volume: fadedPlayback.volume * volumeFactor

        onStatusChanged: {
            console.log (status)
            if (status === Playback.StatusReady) {
                play()
                fadeIn()
                starting()
            }
        }

        function fadeIn() {
            fade.stop()
            fade.from = volumeFactor
            fade.to = 1
            fade.duration = (fade.to - fade.from) * fadeDuration
            fade.start()
            fade.fadingOut = false
        }
        function fadeOut() {
            fade.stop()
            fade.from = volumeFactor
            fade.to = 0
            fade.duration = (fade.from - fade.to) * fadeDuration
            fade.start()
            fade.fadingOut = true
        }

        NumberAnimation on volumeFactor {
            id: fade
            running: false
            property bool fadingOut: false
            onCompleted: {
                if (fadingOut)
                    backend.stop()
            }
        }
    }
}

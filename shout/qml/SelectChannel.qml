import QtQuick 1.1

Rectangle {
    id: selectChannel

    // model with roles "network", "name", "logo", "url" (all strings)
    property variant channels

    property int channelIndex:   -1
    property string channelName: (channels && channels.get(channelIndex)) ? channels.get(channelIndex).name : ""
    property url channelLogo:    (channels && channels.get(channelIndex)) ? channels.get(channelIndex).logo : ""
    property url channelUrl:     (channels && channels.get(channelIndex)) ? channels.get(channelIndex).url  : ""

    property real expanded: 0
    Behavior on expanded { SmoothedAnimation { velocity: 10 } }

    function showMenu() {
        expanded = 1
        var inset = radius * .7 // distance to left / right of popup's geometry from this item's geometry
        var aroundPos = mapToItem(root, inset, 1)
        var aroundSize = Qt.size(width - 2 * inset, height - 1)
        var popup = controller.showPopup
                ({
                     source: 'SelectChannelPopup.qml',
                     //position: mapToItem(root, inset, height - 1),
                     around: Qt.rect(aroundPos.x, aroundPos.y, aroundSize.width, aroundSize.height),
                     size: Qt.size(width - 2 * inset, 150),
                     onClose: function() {
                         expanded = 0
                         root.focus = true
                     }
                 })
        popup.selectChannel = selectChannel
        popup.channels = channels
        popup.fontFamily = root.fontFamily
    }

    gradient: Gradient {
        id: g
        property real top: .8 - .7 * expanded
        property real bottom: .3
        property real alpha: .2 + .1 * m.hovered + .3 * expanded
        function gray(gray, alpha) { return Qt.rgba(gray, gray, gray, alpha) }
        function color(value) { return gray(value * bottom + (1 - value) * top, alpha) }
        GradientStop { position:  0; color: g.color( 0) }
        GradientStop { position: .2; color: g.color(.3) }
        GradientStop { position: .8; color: g.color(.7) }
        GradientStop { position:  1; color: g.color( 1) }
    }

    radius: height / 2
    border { color: Qt.rgba(0, 0, 0, .4 + .4 * m.hovered); width: 1 }
    smooth: true

    Item {
        anchors.fill: parent
        anchors.leftMargin: parent.height * .5
        anchors.rightMargin: parent.height * .5
        clip: true

        Image {
            id: logo

            anchors.left: parent.left
            height: parent.height * .5
            width: height * (implicitWidth / implicitHeight)
            sourceSize: Qt.size(0, height) // proper scaling

            y: expanded - 1 + parent.height * .25

            source: channelLogo
        }

        Text {
            anchors.left: logo.right
            anchors.right: parent.right
            anchors.leftMargin: 12
            height: parent.height
            y: expanded

            verticalAlignment: Qt.AlignVCenter
            text: channelName
            font.family: root.fontFamily
            font.pointSize: parent.height * .45
            color: "white"
        }
    }

    MouseArea {
        id: m
        hoverEnabled: true
        anchors.fill: parent

        property real hovered: containsMouse || expanded
        Behavior on hovered { SmoothedAnimation { velocity: 10 } }

        onClicked: showMenu()
    }
}

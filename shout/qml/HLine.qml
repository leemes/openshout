import QtQuick 1.1

Column {
    width: parent.width

    Rectangle {
        width: parent.width
        height: 1
        color: "black"
        opacity: .3
    }
    Rectangle {
        width: parent.width
        height: 1
        color: "white"
        opacity: .1
    }
}

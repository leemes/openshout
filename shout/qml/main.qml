import QtQuick 1.1

Rectangle {
    id: root

    property string fontFamily: "Sans Serif"

    property string windowTitle: (selectChannel.channelName ? (selectChannel.channelName + "  \u2013  ") : "") + "Shout!"
    onWindowTitleChanged: window.setWindowTitle(windowTitle)

    width: 360
    height: 100
    gradient: Gradient {
        GradientStop { position: 0; color: "#555" }
        GradientStop { position: 1; color: "#333" }
    }

    MouseArea {
        anchors.fill: parent
        property int pressX
        property int pressY
        onPressed: { pressX = mouse.x; pressY = mouse.y }
        onPositionChanged: controller.moveBy(mouse.x - pressX, mouse.y - pressY)
    }

    Column {
        width: parent.width
        y: (parent.height - implicitHeight) / 2
        property real margins: 6
        spacing: 10

        Item {
            x: parent.margins
            width: parent.width - 2 * parent.margins
            height: Math.max(volumeSlider.implicitHeight, loginStatusText.implicitHeight)

            Text {
                id: loginStatusText

                anchors.left: parent.left
                anchors.right: volumeSlider.left
                height: volumeSlider.implicitHeight

                font.family: root.fontFamily
                font.pointSize: 10

                text: "You're not logged in."
                color: "white"
                opacity: .3
            }

            VolumeSlider {
                id: volumeSlider
                anchors.right: parent.right
                y: 2
            }
        }

        SelectChannel {
            id: selectChannel

            x: parent.margins
            width: parent.width - 2 * parent.margins
            height: 36
            channels: channels

            onChannelUrlChanged: playback.play(channelUrl)
        }

        Row {
            x: parent.margins
            width: parent.width - 2 * parent.margins
            spacing: 5

            Image {
                id: statusIcon
                property string icon: playback.buffering ? iconWait : (playback.playing ? iconPlay : iconIdle)
                property string iconPlay: "play"
                property string iconWait: "wait"
                property string iconIdle: "idle"
                source: "../img/icon-" + icon + ".png"
                width: 15
                height: 15
                rotation: 0
                smooth: true
                NumberAnimation on rotation {
                    from: 0; to: 360; loops: Animation.Infinite
                    running: statusIcon.icon == statusIcon.iconWait
                    duration: 1000
                    onRunningChanged: if (!running) statusIcon.rotation = 0
                }
                y: 1
            }

            Text {
                id: statusText

                font.family: root.fontFamily
                font.pointSize: 10
                font.bold: true
                color: "white"

                text: playback.buffering
                      ? "Buffering..."
                      : (playback.playing
                         ? "Playing" + (playback.streamTitle ? ":" : "")
                         : "Idle")
            }

            ScrollText {
                id: streamInfoText
                width: parent.width - statusIcon.width - statusText.width - 2 * parent.spacing

                font.family: root.fontFamily
                font.pointSize: 10
                color: "white"

                text: selectChannel.channelIndex == -1
                      ? "(Select a channel from above)"
                      : playback.streamTitle
            }
        }
    }

    FadedPlayback {
        id: playback
        volume: volumeSlider.value
    }

    ChannelModel {
        id: channels
    }

    focus: true
    Keys.onDownPressed: selectChannel.showMenu()
    Keys.onSpacePressed: selectChannel.showMenu()
    Keys.onReturnPressed: selectChannel.showMenu()
    Keys.onEnterPressed: selectChannel.showMenu()
}

import QtQuick 1.1
import "../js/networks-enabled.js" as Networks

ListModel {
    id: channels

    // Networks (network name => network object)
    property variant networks

    function addNetwork(networkFilePrefix) {
        if (new RegExp('^[a-z0-9/_]+$').exec(networkFilePrefix) === null) {
            console.log('Failed to load network "' + networkFilePrefix + '". Network file name prefix has to match the reg exp /[0-9/_]+/.')
        } else {
            var networkObject =
                    Qt.createQmlObject('import "../js/networks/' + networkFilePrefix + '.js" as Network\n' +
                                       'import QtQuick 1.1\n' +
                                       'QtObject {\n' +
                                       '    property string name: Network.name\n' +
                                       '    property url homepage: Network.homepage\n' +
                                       '    property url logo: Network.logo ? Network.logo : "../js/networks/' + networkFilePrefix + '.png"\n' +
                                       '    function init(callback) { Network.init(callback); }\n' +
                                       '}\n',
                                       channels)

            // Add network to the property `networks`
            var newNetworks = networks
            newNetworks[networkObject.name] = networkObject
            networks = newNetworks

            // Add channels to the model via callback function (asynchronously)
            networkObject.init(function(networkObject) {
                return function(channel) {
                    channels.append({
                                        network: networkObject.name,
                                        name: channel.name,
                                        logo: (channel.logo ? channel.logo : networkObject.logo),
                                        url: channel.url
                                    })
                }
            }(networkObject))
        }
    }

    Component.onCompleted: {
        networks = {}
        for(var i = 0; i < Networks.networks.length; ++i)
            addNetwork(Networks.networks[i])
    }
}

import QtQuick 1.1

Item {
    id: scrollText

    property string text
    property string separator: "   \u2013   "
    property real pixelsPerSecond: 30

    property alias font: t.font
    property alias color: t.color

    implicitHeight: t.height
    clip: true

    Text {
        id: dummy
        visible: false
        font: t.font
        text: scrollText.text
    }

    Text {
        id: t
        text: scrollingEnabled
              ? scrollText.text + scrollText.separator + scrollText.text + scrollText.separator
              : scrollText.text

        property bool scrollingEnabled: (dummy.implicitWidth > scrollText.width)
        property bool currentlyScrolling: false

        Timer {
            id: animationStarter
            interval: 1500
            running: false
            repeat: false
            onTriggered: t.currentlyScrolling = t.scrollingEnabled
        }

        Timer {
            id: animation
            interval: 1000 / pixelsPerSecond
            running: t.currentlyScrolling && !mouse.pressed
            repeat: true
            onTriggered: {
                t.scroll(-1)
            }
        }

        function scroll(relativeX) {
            var mod = t.implicitWidth / 2; // movement modulo
            t.x = (t.x + relativeX + 2 * mod) % mod - mod;
        }
    }

    MouseArea {
        id: mouse
        anchors.fill: parent
        enabled: t.scrollingEnabled
        property real oldX
        onPressed: {
            oldX = mouseX
        }
        onMouseXChanged: {
            t.scroll(mouseX - oldX)
            oldX = mouseX
        }
    }

    onTextChanged: resetScrolling()
    onWidthChanged: resetScrolling()

    function resetScrolling() {
        t.x = 0
        t.currentlyScrolling = false
        animationStarter.stop()
        animationStarter.start()
    }
}

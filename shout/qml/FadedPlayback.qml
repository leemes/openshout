import QtQuick 1.1

Item {
    property real volume: 1.0

    // status:
    property bool playing: p1.playing || p2.playing
    property bool buffering: p1.buffering || p2.buffering
    property string streamTitle: pCurrent.playing ? pCurrent.streamTitle : ""

    // toggling between single playbacks:
    property variant pNext: p1
    property variant pCurrent: (pNext === p1) ? p2 : p1

    function play(url) {
        // toggle:
        pNext = pCurrent
        // play:
        pCurrent.play(url);
    }

    FadedPlaybackSingle {
        id: p1
        volume: parent.volume
        onStarting: p2.stop()
    }
    FadedPlaybackSingle {
        id: p2
        volume: parent.volume
        onStarting: p1.stop()
    }
}

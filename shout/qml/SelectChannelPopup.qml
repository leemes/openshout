import QtQuick 1.1

Rectangle {
    width: client.width
    height: client.height

    property variant selectChannel
    property variant channels
    property string fontFamily

    function accept() {
        selectChannel.channelIndex = list.currentIndex
        controller.close()
    }
    function reject() {
        controller.close()
    }

    color: "#333"
    border { color: "black"; width: 2 }

    ListView {
        id: list

        anchors.fill: parent
        anchors.margins: 1
        clip: true

        model: channels
        focus: true
        highlightMoveDuration: 200

        section {
            property: 'network'
            delegate: Rectangle {
                height: 20
                color: Qt.rgba(1, 1, 1, .2)
                width: parent.width
                Row {
                    anchors.fill: parent
                    anchors.margins: 4
                    spacing: 10
                    Image {
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        width: (implicitWidth / implicitHeight) * height
                        source: channels.networks[section].logo
                        sourceSize: Qt.size(0, height) // proper scaling
                    }
                    Text {
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        verticalAlignment: Qt.AlignVCenter
                        font.family: fontFamily
                        font.pointSize: 10
                        font.bold: true
                        text: section
                    }
                }
            }
        }

        delegate: Rectangle {
            height: label.implicitHeight + 2
            width: parent.width
            color: Qt.rgba(1, 1, 1, .1 * ma.containsMouse)
            Text {
                id: label
                anchors.fill: parent
                anchors.margins: 4 // only horizontally, because the whole item adapts to this text's implicitWidth
                text: name
                font.family: fontFamily
                color: Qt.rgba(1, 1, 1, (list.currentIndex == index) ? 1 : .7)
                font.pointSize: 10
                verticalAlignment: Qt.AlignVCenter
            }
            MouseArea {
                id: ma
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    list.currentIndex = index
                    accept();
                }
            }
        }

        highlight: Item {
            Rectangle {
                height: parent.height
                width: parent.width + 4
                x: -2
                gradient: Gradient {
                    GradientStop { position: .1; color: Qt.rgba(.2, .2, .2, .75) }
                    GradientStop { position: .9; color: Qt.rgba(0, 0, 0, .75) }
                }
                border { color: Qt.rgba(1, 1, 1, .5); width: 1 }
            }
        }

        Keys.onReturnPressed: accept()
        Keys.onEscapePressed: reject()

        Keys.onPressed: {
            var i = 0;
            if (event.key === Qt.Key_PageDown)
                moveCurrentIndexBy(7)
            else if (event.key === Qt.Key_PageUp)
                moveCurrentIndexBy(-7)
            else if (event.key === Qt.Key_Home)
                currentIndex = 0
            else if (event.key === Qt.Key_End)
                currentIndex = (count - 1)
        }

        function moveCurrentIndexBy(amount) {
            currentIndex = Math.min(count - 1, Math.max(0, currentIndex + amount))
        }
    }

    onSelectChannelChanged: {
        // select the index of the current channel
        list.currentIndex = selectChannel.channelIndex
    }
}

import QtQuick 1.1

Item {
    id: volumeSlider

    property real value: 1.0 // No property binding (otherwise, it will break if value changed from outside)

    implicitWidth: backgroundImage.implicitWidth
    implicitHeight: backgroundImage.implicitHeight

    property real smoothedValue: 1.0
    Behavior on smoothedValue { SmoothedAnimation { velocity: 3 } }
    property bool externalChange: true
    onSmoothedValueChanged: {
        externalChange = false
        value = smoothedValue
        externalChange = true
    }
    onValueChanged: {
        if (externalChange)
            smoothedValue = value
    }


    Image {
        id: backgroundImage

        width: volumeSlider.width
        height: volumeSlider.height
        source: "../img/volume.png"
        opacity: .1
    }
    Item {
        id: foregroundWrapper

        width: volumeSlider.width * volumeSlider.value
        height: volumeSlider.height
        clip: true
        Image {
            id: foregroundImage
            width: volumeSlider.width
            height: volumeSlider.height
            source: backgroundImage.source
        }
    }

    Text {
        anchors.right: parent.left
        anchors.rightMargin: 5
        height: volumeSlider.implicitHeight
        verticalAlignment: Qt.AlignVCenter

        font.family: root.fontFamily
        font.pointSize: 7

        visible: mouse.containsMouse || mouse.pressed
        text: "volume: " + Math.round(volumeSlider.value * 100) + "%"
        color: "white"
    }

    MouseArea {
        id: mouse

        anchors.fill: parent
        hoverEnabled: true

        onPressed: drag()
        onMouseXChanged: if (mouse.buttons) drag()

        function drag() {
            smoothedValue = Math.min(1.0, Math.max(0.0, mouseX / width))
        }
    }
}

#ifndef PLAYBACKMPLAYER_H
#define PLAYBACKMPLAYER_H

#include "playback.h"

class QProcess;

class PlaybackMplayer : public AbstractPlayback
{
    Q_OBJECT
public:
    explicit PlaybackMplayer(QObject *parent = 0);
    ~PlaybackMplayer();

protected:
    void _load() Q_DECL_OVERRIDE;
    void _setVolume() Q_DECL_OVERRIDE;
    void _togglePause() Q_DECL_OVERRIDE;

    virtual bool mplayerSend(const QString &command);
    virtual void mplayerReceive(const QString &message);

    QProcess *mplayer;

private slots:
    void mplayerProcessData();
};

#endif // PLAYBACKMPLAYER_H

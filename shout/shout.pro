# Add more folders to ship with the application, here
folder_01.source = qml
folder_01.target = .
folder_02.source = img
folder_02.target = .
folder_03.source = js
folder_03.target = .
DEPLOYMENTFOLDERS = folder_01 folder_02 folder_03

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

# Qt4 compatibility
lessThan(QT_MAJOR_VERSION, 5) {
    QT += script
    DEFINES += Q_DECL_OVERRIDE=
    DEFINES += Q_DECL_FINAL=
}


SOURCES += main.cpp \
    playback.cpp \
    playbackmplayer.cpp \
    qmlpopup.cpp \
    qmlcontroller.cpp

HEADERS += \
    playback.h \
    playbackmplayer.h \
    qmlpopup.h \
    qmlcontroller.h

OTHER_FILES += \
    qml/FadedPlayback.qml \
    qml/main.qml \
    qml/ScrollText.qml \
    qml/HLine.qml \
    qml/FadedPlaybackSingle.qml \
    js/networks/di_fm.js \
    js/networks-enabled.js \
    js/networks/_common.js \
    js/networks/jazzradio.js

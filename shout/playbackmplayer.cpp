#include "playbackmplayer.h"
#include <QProcess>

PlaybackMplayer::PlaybackMplayer(QObject *parent) :
    AbstractPlayback(parent),
    mplayer(new QProcess(this))
{
    QStringList args;
    args << "-slave";
    args << "-quiet";
    args << "-quiet";
    args << "-idle";
    // args << "-softvol";
    args << "-volume" << "0";
    mplayer->start("mplayer", args);

    if (mplayer->waitForStarted()) {
        connect(mplayer, SIGNAL(readyRead()), SLOT(mplayerProcessData()));
    } else {
        qFatal("Failed to initialize mplayer backend: %s\nIs mplayer installed on your system (and in PATH)?", qPrintable(mplayer->errorString()));
    }
}

PlaybackMplayer::~PlaybackMplayer()
{
    // Strategy to shutdown MPlayer:
    // 1. Send quit command (normally, this should be enough!)
    // 2. If mplayer doesn't finish, send SIGTERM (does nothing on Win)
    // 3. If mplayer doesn't finish, send SIGKILL (sends TerminateProcess on Win)
    mplayerSend("quit");
    if (!mplayer->waitForFinished()) {
        qDebug("Mplayer doesn't quit... Tryping to terminate...");
        mplayer->terminate();
        if (!mplayer->waitForFinished()) {
            qDebug("Mplayer doesn't quit... Tryping to kill...");
            mplayer->kill();
        }
    }
}

void PlaybackMplayer::_load()
{
    setStreamTitle(QString());
    if (status() != StatusIdle) {
        setVolume(0);
        pause();
    }

    if (!m_url.isValid()) {
        mplayerSend("stop");
        setStatus(StatusIdle);
    }
    else if (m_url.toString().endsWith(".pls")
             || m_url.toString().endsWith(".asx")
             || m_url.toString().endsWith(".m3u")) {
        mplayerSend("loadlist " + m_url.toString());
        setStatus(StatusRequesting);
    }
    else {
        mplayerSend("loadfile " + m_url.toString());
        setStatus(StatusRequesting);
    }

    pause();
}

void PlaybackMplayer::_setVolume()
{
    mplayerSend("set volume " + QString::number(m_volume * 100.0));
}

void PlaybackMplayer::_togglePause()
{
    mplayerSend("pause");
}

bool PlaybackMplayer::mplayerSend(const QString &command)
{
    qDebug("%p mplayer <- %s", this, qPrintable(command));

    QByteArray data = command.toUtf8();
    data += "\n";

    return mplayer->write(data) == data.length()
            && mplayer->waitForBytesWritten();
}

void PlaybackMplayer::mplayerReceive(const QString &message)
{
    qDebug("%p mplayer -> %s", this, qPrintable(message));

    if (message.startsWith("Cache fill:"))
        setStatus(StatusBuffering);

    if (message == "Starting playback...")
        setStatus(StatusReady);

    // FIXME: For FLV streams, mplayer receives the stream title of the track first played, but no further stream titles. This parser has been commented out so currently no stream titles will be used for FLV streams.
    if (message.startsWith("ICY Info:")) { // || message.startsWith("StreamTitle: ")) {
        QRegExp re1("StreamTitle='(.*)';"); re1.setMinimal(true);
        //QRegExp re2("StreamTitle: (.*)");

        if (re1.indexIn(message) != -1)
            setStreamTitle(re1.cap(1));
        //if (re2.indexIn(message) != -1)
        //    setStreamTitle(re2.cap(1));
    }
}

void PlaybackMplayer::mplayerProcessData()
{
    while (mplayer->bytesAvailable())
        mplayerReceive(mplayer->readLine().trimmed());
}
